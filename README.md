# workinghours

Log for how long you have been attached to a tmux session

As the name suggests, this is intended to be used to see how many hours you have worked.

## Usage
**Warning:** *You have to detach before ending the session! Otherwise the last session duration will not be logged.*

Start the tmux session with `workinghours --init`.
This will create a session called 'work' by default and set up hooks to call `workinghours` with the appropriate parameters when attaching/detaching the session.
Default session name is `work` with all files using `workinghours`.
You can provide a different session name with `--session`.

When attaching a timestamp will be saved to `/.workinghours/workinghours_starttime`.
When detaching this file will be removed and the total will be added to to the summary file.
This only applies if your are the last/first client for this session.
The summary is written to `/.workinghours/workinghours`.
You can change the path in `workinghours` by setting the `path` variable in your `~/.workinghoursrc`.

While working use `workinghours` to get current duration for the running session - can be combined with `--session`.

Call `workinghours` with the `--new-week` parameter to archive the current database file.
The file is moved to `.workinghours/archive/$YEAR/$WEEK`, where `$YEAR` is the year and `$WEEK` is the ISO-8601 number of the week for the week _before_ (Rational is, that you run it on Mondays, when starting work.).
The `--new-week` parameter needs an additional numeric parameter representing the amount of hours you should have worked in the last week.
With this parameter your over-all overtime is calculated and saved in `overtime`.
Use `--week-number` to use a different week number then the one from the week before.
Use `--new-week -1` to not calculate overtime.
If `.workinghours` is a git repository (contains a directory named `.git`) then all files inside `.workinghours` will be staged and committed.

If you forgot to archive your working hours on Monday use the `--before` option to only archive logged hours _before_ the given date.
Already logged times from the same day will not be archived.
Example: `--before 2019-12-23` will archive all entries made before the 2019-12-23 not including those from the 23rd of December.

### Example
Start a new session for your a project
```
workinghours -s sideproject -i
```

Check for how long you've been working without a break
```
workinghours -s sideproject
```

Detach, make sure session is closed and see how long you worked
```
workinghours -s sideproject
```

Archive the last week (supposing you should work 40 hours and there was one holiday)
```
workinghours -s sideproject -w 32
```

Archive the last week (supposing there is no mandatory amount of hours to work)
```
workinghours -s sideproject -w 0
```

Archive the week before your vacation
(supposing you should work 40 hours and the last week working was number 17)
```
workinghours -s sideproject -w 40 --week-number 17
```

After your vacation you realize on Wednesday that you forgot to archive your working hours before your vacation
(supposing you should work 40 hours and the last week working was number 17)
```
workinghours -s sideproject -w 40 --week-number 17 --before 2019-05-06
```

Create a new session while being attached to a running remote session
```
workinghours -s secondSession -i --no-attach
```

You can group summary files in subdirectories by using the `--customer` option.
This will create a directory `big_customer` in `~/.workinghours` and store the `current_project` file in there.
```
workinghours -s current_project -c big_customer -i
```

## Configuration
If a `~/.workinghoursrc` is present it will be sourced.
To overwrite default options just set the according variables there.
As it is sourced, you need to use valid bash syntax.

**As `.workinghoursrc` is sourced anyone with write access could insert malicious commands - consider setting `600` as permission.**

Example:

```
path="$HOME/.myworkinghours"
session="mywork"
sessionprefix="myworkinghours"
script="$HOME/bin/workinghours.sh"
```

## Zsh Completion

`workinghours` includes a simple Zsh completion file.
It's recommended to symlink to it from somewhere within your `fpath`.
This way you will always have the latest version when updating.

## TO-DO
  - create weekly / monthly / yearly summaries
  - Why the session-closed hook doesn't work?

** All branches except `master` and `develop` will be force pushed, don't rely on them! **
