#!/bin/bash

# Log a time delta
#
# Log file format:
# start time, stop time, delta, accumulated deltas from current file
#
# Date format is ISO8601
# Delta format is HH:MM:SS
# Total format is HH:MM:SS

read -r -d '' helptext << EOT
Usage: workinghours [OPTION] [ -s | --session ]
Log for how long you have been attached to a tmux session

Without any options or with --session only print a status message.

Mandatory arguments to long options are mandatory for short options too.
  -i, --init                  set up a tmux session and attach
      --no-attach             do not attach to session (only with --init)
  -s, --session NAME          choose a session name (default: workinghours)
  -c, --customer NAME         save files in subfolder NAME
  -w, --new-week HOURS        archive the current database file with number of
                              last week into archive directory
                              HOURS is the amount of hours your were supposed
                              to work
      --before DATE           only archive database entries older than DATE
                              date format: YYYY-MM-DD
                              (only in combination with --new-week)
      --week-number number    use given number instead of last week's
      --last-year             force use of last year to archive (only in
                              combination with --week-number)
      --start                 write a timestamp to a temp file
      --stop                  read timestamp from temp file, calculate delta and
                              write output to file
  -h, --help                  show this help
Any option can be combined with --session option.
Furthermore, --new-week can be combined with --week-number option - which can be
combined with --last-year.
EOT

# Define options in long and short form
SHORT=c:his:tw:
LONG=customer:,help,init,new-week:,no-attach,session:,start,stop,today,week-number:,last-year,before:

# Option parsing shamelessly copied from
# https://stackoverflow.com/a/29754866/8699120
#
# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
#  exit 2 when getopt has complained about wrong arguments to stdout
PARSED=$(getopt --options $SHORT --longoptions $LONG --name "$0" -- "$@") || exit 2

# use eval with "$PARSED" to properly handle the quoting
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -i|--init)
            init=y
            shift
            ;;
        -h|--help)
            echo "$helptext"
            exit 0
            shift
            ;;
        -c|--customer)
            customer=$2
            shift 2
            ;;
        -w|--new-week)
            week=y
            target=$2
            shift 2
            ;;
        --no-attach)
            noattach=y
            shift
            ;;
        -s|--session)
            session=$2
            shift 2
            ;;
        --start)
            begin=y
            shift
            ;;
        --stop)
            end=y
            shift
            ;;
        -t|--today)
            today=y
            shift
            ;;
        --week-number)
            weekno=$2
            shift 2
            ;;
        --last-year)
            lastyear=y
            shift
            ;;
        --before)
            before=$2
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            >&2 echo "Unknown option $1"
            exit 1
            ;;
    esac
done

errxit(){
    printf "%s\n" "$*" >&2;
    exit 1;
}

if [[ -z "$path" ]]; then
    path="$HOME/.workinghours"
fi

if [[ -e $HOME/.workinghoursrc ]]; then
    # Disable ShellCheck error message
    # ShellCheck will complain if file is not present, which is default.
    # shellcheck disable=SC1091
    # ShellCheck is not able to check files with non-const path
    # shellcheck source=/dev/null
    source "$HOME"/.workinghoursrc
fi

# session etc could be set in workinghoursrc, so source first
if [[ -z $session ]] || [[ $session = work ]]; then
    session="work"
    sessionprefix="workinghours"
else
    sessionprefix="$session"
fi

if [[ -n $customer ]]; then
    path=${path}/${customer}
fi

outputfile="${path}/${sessionprefix}"
starttimefile="${outputfile}_starttime"
overtimefile="${outputfile}_overtime"

convertfromseconds(){
    printf '%02d:%02d:%02d\n' $(($1/3600)) $(($1%3600/60)) $(($1%60))
}

converttosecondes(){
    echo "$1" | awk -F: '{ print ($1 * 3600) + ($2 * 60) + $3 }'
}

# read total time from file and convert to seconds
lasttotaltime(){
    local ltt
    if [[ -w $outputfile ]]; then        # FILE exists and write permission is granted
        ltt=$(tail -1 "$outputfile" | awk '{print $NF}')
        ltt=$(converttosecondes "$ltt")
    else
        ltt=0
    fi
    echo "$ltt"
}

# sets multiple global variables
sessiontimes(){
    [[ -r $starttimefile ]] || errxit "Can't read $starttimefile"
    endstamp=$(date +%s)
    startstamp=$(< "$starttimefile")
    starttime=$(date --iso-8601=seconds -d @"$startstamp")
    endtime=$(date --iso-8601=seconds -d @"$endstamp")
    secondspassed=$((endstamp - startstamp))
    lasttotaltime=$(lasttotaltime)
    totaltime=$((lasttotaltime + secondspassed))
    totaltime=$(convertfromseconds "$totaltime")
    diff=$(convertfromseconds "$secondspassed")
    result="$starttime $endtime $diff $totaltime"
    echo "$result"
}

start(){
    # Hook is run after attaching
    if [[ $(tmux list-clients -t "$session" | wc -l) -eq 1 ]]; then
        [[ ! -e $starttimefile ]] || errxit "$starttimefile exists"
        date +%s > "$starttimefile" || errxit "Can't write $starttimefile"
    fi
}

stop(){
    # Hook is run after detaching
    if [[ $(tmux list-clients -t "$session" | wc -l) -eq 0 ]]; then
        sessiontimes >> "$outputfile" || errxit "Can't write $outputfile"
        rm "$starttimefile"
    fi
}

status(){
    if [[ ! -f $outputfile && ! -f $overtimefile && ! -f $starttimefile ]]; then
        errxit "No information for session $session found"
    fi
    if [[ $(tmux list-clients -t "$session" 2>/dev/null | wc -l) -eq 0 ]]; then
        [[ ! -e $starttimefile ]] || errxit "No session, but starttimefile exists"
        echo '*****     You are not working!     *****'
        if [[ -f $outputfile ]]; then
            lastday=$(tail -1 "$outputfile" | cut -d" " -f1 | date --iso-8601 -f -)
            lastdaytotal=$(daytotal "$lastday")
            total=$(tail -1 "$outputfile" | cut -d" " -f4 -)
            echo "Last day's total:                $lastdaytotal"
            echo "Total:                           $total"
        fi
    else
        [[ -e $starttimefile ]] || errxit "No starttimefile, though session exists"
        echo 'Begin                     Now                       Duration Total'
        stimes=$(sessiontimes)
        echo "$stimes"
        if [[ -e $outputfile ]]; then
            todaytotal=$(today)
            if [[ $todaytotal != "00:00:00" ]]; then
                total=$(tail -1 "$outputfile" | cut -d" " -f4)
                st=$(echo "$stimes" | cut -d" " -f3)
                sts=$(converttosecondes "$st")
                ts=$(converttosecondes "$total")
                total=$(convertfromseconds "$((ts + sts))")
                printf "Today's total / overall total %30s %s\n" "$todaytotal" "$total"
            else
                printf "%41s\n" "$total"
            fi
        fi
    fi
    if [[ -e $overtimefile ]]; then
        echo "Overtime without this week's:  $(<"$overtimefile")"
    fi
    exit 0
}

today(){
    day=$(date --iso-8601=date)
    total=$(daytotal "$day")
    if [[ $total != "00:00:00" ]]; then
        echo "$total"
    else
        lastday=$(tail -1 "$outputfile" | cut -d" " -f1 | date --iso-8601 -f -)
        lastdaytotal=$(daytotal "$lastday")
        echo "*** You did not work today! ***"
        echo "Total for $lastday:  $lastdaytotal"
    fi
}

daytotal(){
    times=$(grep "$1" "$outputfile" | cut -d" " -f 3)
    if [[ -r $starttimefile ]]; then
        sum=$(converttosecondes "$(sessiontimes | cut -d" " -f3)")
    else
        sum=0
    fi
    for t in $times; do
        sec=$(converttosecondes "$t")
        sum=$((sec + sum))
    done
    convertfromseconds $sum
}

overtime(){
    lasttotaltime=$(lasttotaltime)
    diff=$((lasttotaltime - target))
    if [[ -w $overtimefile ]]; then
        lastovertime=$(< "$overtimefile")
        if [[ ${lastovertime::1} = - ]]; then
            lastnegativ=1
        else
            lastnegativ=0
        fi
        lastovertime=${lastovertime:2:8}
        lastovertime=$(converttosecondes "$lastovertime")
    else
        lastovertime=0
    fi

    if [[ $lastnegativ -eq 1 ]];then
        lastovertime=$((lastovertime * -1))
    fi

    newovertime=$((lastovertime + diff))
    if [[ -e $overtimefile ]]; then
        cp "$overtimefile" "${overtimefile}.1"
    fi
    if [[ $newovertime -lt 0 ]]; then
        newovertime=$((newovertime * -1))
        newovertime=$(convertfromseconds $newovertime)
        echo - "$newovertime"
        echo - "$newovertime" > "$overtimefile"
    else
        echo + "$(convertfromseconds "$newovertime")"
        echo + "$(convertfromseconds "$newovertime")" > "$overtimefile"
    fi
}

splitfile(){
    local lasttotaltime=0
    before_date="$1"
    first_date=$(head -1 "$outputfile" | cut -d" " -f1 | date --iso-8601 -f -)
    last_date=$(tail -1 "$outputfile" | cut -d" " -f1 | date --iso-8601 -f -)

    if [[ $first_date > $before_date ]] || [[ $last_date < $before_date ]]; then
        return
    fi

    [[ ! -e ${outputfile}.arc ]] || errxit "File exists: ${outputfile}.arc"
    [[ ! -e ${outputfile}.new ]] || errxit "File exists: ${outputfile}.new"
    cp "$outputfile" "${outputfile}.bak"

    while read -r line ; do
        line_date=$(echo "$line" | cut -d" " -f1 | date --iso-8601 -f -)
        if [[ $line_date < $before_date ]]; then
            echo "$line" >> "${outputfile}.arc"
        else
            starttime=$(echo "$line" | cut -d" " -f1)
            endtime=$(echo "$line" | cut -d" " -f2)
            startstamp=$(date +%s -d "$starttime")
            endstamp=$(date +%s -d "$endtime")
            secondspassed=$((endstamp - startstamp))
            lasttotaltime=$((lasttotaltime + secondspassed))
            totaltime=$(convertfromseconds "$lasttotaltime")
            diff=$(convertfromseconds "$secondspassed")
            result="$starttime $endtime $diff $totaltime"
            echo "$result" >> "${outputfile}.new"
        fi
    done < "${outputfile}"

    if ! [[ -e "${outputfile}.arc" && -e "${outputfile}.new" ]]; then
        rm "${outputfile}".{arc,new} > /dev/null 2>&1
        errxit "Not a proper split, aborting!"
    fi
    mv "${outputfile}.arc" "$outputfile"
}

newweek(){
    [[ -r $outputfile ]] || errxit "File not found: $outputfile"

    target=$(bc <<< "$target * 3600")
    target=${target%.*} # convert to int

    if [[ $target -eq -1 ]] && [[ -e $overtimefile ]]; then
        echo "You have a overtimefile and provide target hours as -1!" >&2
        echo "Aborting!" >&2
        exit 1
    fi

    # archive with number of last week if --week-number not given
    lastweekno=$(date -d '-7day' +%V)
    year="$(date -d '-7day' +%Y)"
    if [[ ! $weekno ]]; then
        weekno=$lastweekno
    else
        if [[ $weekno -gt $lastweekno ]]; then
            if [[ -z $lastyear ]]; then
                echo "Current week numbers is less than provided!"
                echo "$weekno > $lastweekno" >&2
                echo "Use --last-year" >&2
                echo "Aborting..." >&2
                exit 1
            else
                year=$(date +%Y -d 'last year')
            fi
        fi
    fi
    archivepath="${path}/archive/$year"
    archivefile="${archivepath}/${sessionprefix}-${weekno}"
    [[ ! -e $archivefile ]] || errxit "$archivefile exists"

    if [[ ! -d $archivepath ]]; then
        mkdir -p "$archivepath"
    fi

    if [[ -z $before ]]; then
        before=$(date --iso-8601)
    fi

    first_date=$(head -1 "$outputfile" | cut -d" " -f1 | date --iso-8601 -f -)
    [[ $first_date != "$before" ]] || errxit "Nothing to archive"

    cd "$path" || exit 1
    if [[ -d ".git" ]]; then
        if ! command -v git &> /dev/null; then
           errxit "Git repository found, but no executable"
        fi
        git add . > /dev/null
        git commit -m "Before archiving week $weekno" > /dev/null
    fi

    splitfile "$before"

    if [[ $target -ne -1 ]]; then
        overtime
    fi

    mv "${outputfile}" "$archivefile"
    if [[ -e "${outputfile}.new" ]]; then
        mv "${outputfile}.new" "${outputfile}"
    fi
}

scriptlocation(){
    if [[ -z $script ]]; then
        script="$(command -v workingshours > /dev/null)"
        [[ -x $script ]] || script=""
    fi
    if [[ -z $script ]]; then
        script=${BASH_SOURCE[0]}
        [[ -x $script ]] || script=""
    fi
    if [[ -z $script ]]; then
        echo "Could not determine script location."
        echo "Add it to PATH or set it in .workinghoursrc"
        exit 1
    fi
}

init(){
    scriptlocation

    [[ -d $path ]] || mkdir -p "$path" || exit 1
    export SHLVL=$((SHLVL - 1))
    tmux new-session -s "$session" -d || exit 1

    if [[ -n $customer ]]; then
        customeropt="-c $customer"
    fi

    tmux set-hook -t "$session" client-attached "run-shell -b \"$script --start --session $session $customeropt\""
    tmux set-hook -t "$session" client-detached "run-shell -b \"$script --stop --session $session $customeropt\""
    tmux set-hook -t "$session" session-closed "run-shell -b \"$script --stop --session $session $customeropt\""

    if [[ ! $noattach ]]; then
        # attach to the new session
        tmux -2 attach -t "$session"
    fi
}

if [[ -n $init ]]; then
    init
else
    # init creates the path if it doesn't exist
    # checking after init makes sure there is no confusion when changing the config
    # after a session is initialized
    [[ -d $path ]] || errxit "$path doesn't exist"

    if [[ -n $begin ]]; then
        start
    elif [[ -n $end ]]; then
        stop
    elif [[ -n $week ]]; then
        newweek "$target"
    elif [[ -n $today ]]; then
        today
    else
        status
    fi
fi

exit 0
